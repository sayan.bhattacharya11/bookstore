from django.db.models.signals import pre_save
from django.dispatch import receiver
from .models.subscription import Subscription


@receiver(pre_save, sender = Subscription)
def book_count_handler(sender, instance, **kwargs):
	qs = Subscription.objects.filter(returned=False)
	books_available = instance.book.count - len(qs)
	if  books_available > 0:
		instance.book.count -= 1
	return