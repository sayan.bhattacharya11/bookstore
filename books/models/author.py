from django.db import  models


class Author(models.Model):
	id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=25)
	address = models.TextField(max_length=100)