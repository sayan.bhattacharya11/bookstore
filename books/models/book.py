from django.db import  models
from ..models.author import  Author


class Book(models.Model):
	title =  models.CharField(max_length=30)
	description = models.TextField()
	count = models.PositiveSmallIntegerField()
	subscription_cost = models.IntegerField()
	topic = models.CharField(max_length=20)
	author = models.ManyToManyField(Author)
