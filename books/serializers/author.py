from rest_framework_json_api import  serializers
from rest_framework_json_api.relations import ResourceRelatedField
from ..models.author import Author



class AuthorSerializer(serializers.ModelSerializer):

	class Meta:
		model = Author
		fields = "__all__"



