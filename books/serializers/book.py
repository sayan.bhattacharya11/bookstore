from rest_framework_json_api import  serializers
from rest_framework_json_api.relations import ResourceRelatedField
from ..models.book import Book,Author
from ..serializers.author import AuthorSerializer


class BookSerializer(serializers.ModelSerializer):
	included_serializers = {
		'author': AuthorSerializer
	}

	author = ResourceRelatedField(queryset=Author.objects, many=True)

	class Meta:
		model = Book
		fields = "__all__"
