from rest_framework_json_api import  serializers
from django.contrib.auth.models import User
from rest_framework_json_api.relations import ResourceRelatedField
from ..models.subscriber import Subscriber
from books.serializers.user import UserSerializer


class SubscriberSerializer(serializers.ModelSerializer):
	included_serializers = {
		"user": UserSerializer
	}
	# include the User serializer

	user = ResourceRelatedField(queryset=User.objects)

	class Meta:
		model = Subscriber
		fields = "__all__"

