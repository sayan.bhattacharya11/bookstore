from rest_framework import viewsets
from ..models.author import Author
from ..serializers.author import AuthorSerializer


class AuthorViewset(viewsets.ModelViewSet):
	queryset = Author.objects.all()
	serializer_class = AuthorSerializer
	ordering = ["id"]
	filterset_fields = "__all__"
	# "__all__"
	search_fields = ('id', 'name')
