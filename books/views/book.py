from rest_framework import viewsets
from ..models.book import Book
from ..serializers.book import BookSerializer


class BookViewset(viewsets.ModelViewSet):
	queryset = Book.objects.all()
	serializer_class = BookSerializer
	ordering = ["id"]

	filterset_fields = "__all__"

	search_fields = ('id', 'title','description')