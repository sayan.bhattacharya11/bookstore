import datetime
from rest_framework import viewsets
from rest_framework.response import Response


from ..models.subscription import Subscription
from ..serializers.subscription import SubscriptionSerializer


class SubscriptionViewset(viewsets.ModelViewSet):
	queryset = Subscription.objects.all()
	serializer_class = SubscriptionSerializer
	ordering = ["borrowed_date"]

	filterset_fields = "__all__"
	search_fields = ('id','amount_paid')


class SubscriptionExpiredViewset(viewsets.ViewSet):

	def list(self, request):
		queryset = Subscription.objects.filter(returned=False)
		format_str = '%Y%m%d'
		qs = ""
		for element in queryset:
			date = str(element.borrowed_date).split("-")
			days_elapsed = int(date[2]) + element.days
			date.pop()
			date.append(str(days_elapsed))
			date_str = "".join(date)
			datetime_obj = datetime.datetime.strptime(date_str, format_str)
			difference = datetime.date.today() - datetime_obj.date()
			days = difference.days
			if days < 0:
				qs = queryset.exclude(id=element.id)
		serializer = SubscriptionSerializer(qs, many=True)
		return Response(serializer.data)
