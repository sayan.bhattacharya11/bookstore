from rest_framework import viewsets
from ..models.subscriber import Subscriber
from ..serializers.subscriber import SubscriberSerializer


class SubscriberViewset(viewsets.ModelViewSet):
	queryset = Subscriber.objects.all()
	serializer_class = SubscriberSerializer
	ordering = ["id"]

	filterset_fields = "__all__"
	search_fields = ('id', 'user')


