from django.test import TestCase
from rest_framework import status
from django.contrib.auth.models import User
from books.models import Subscriber

CONTENT_TYPE = "application/vnd.api+json"


class SubscriberTest(TestCase):

	@classmethod
	def setUp(cls):
		user = User.objects.create(username="riemann")
		Subscriber.objects.create(user=user, address="gjhgj", phone_number="7890987654")

	def test_phone_number_length(self):
		subscriber = Subscriber.objects.get(id=1)
		max_length = subscriber._meta.get_field('phone_number').max_length
		self.assertEquals(max_length, 17)

	def test_check_user(self):
		subscriber = Subscriber.objects.get(id=1)
		user = subscriber.user.username
		self.assertEqual(user, "riemann")

	def test_subscriber_api(self):
		response = self.client.get("/api/subscriber/", content_type=CONTENT_TYPE)
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_subscriber_address(self):
		subscriber = Subscriber.objects.get(id=1)
		field_label = subscriber._meta.get_field('address').verbose_name
		self.assertEquals(field_label, "address")

