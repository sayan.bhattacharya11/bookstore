import json
from rest_framework.test import APITestCase
from rest_framework import status
from django.test import TestCase
from books.models import Book, Author

CONTENT_TYPE = "application/vnd.api+json"


class BookTest(TestCase):

	@classmethod
	def setUp(cls):
		author1 = Author.objects.create(name='James Joyce',address="45 Leicester City")

		book = Book.objects.create(title="Ullyses",description="greek myth",count=4,subscription_cost=40,
		                           topic="Magical Realism")

		book.author.add(author1)

	def test_book_title_length(self):
		book = Book.objects.get(id=1)
		title = book._meta.get_field('title').max_length
		self.assertEquals(title, 30)

	def test_book_title(self):
		book = Book.objects.get(id=1)
		field_label = book._meta.get_field('title').verbose_name
		self.assertEquals(field_label,"title")

	def test_book_topic(self):
		book = Book.objects.get(id=1)
		field_label = book._meta.get_field('topic').max_length
		self.assertEquals(field_label, 20)

	def test_book_author(self):
		book = Book.objects.get(id=1)
		author = ""
		for el in book.author.all():
			author = el.name
		author_name = Author.objects.get(id=1)
		self.assertEqual(author,author_name.name)

	def test_book_create_api(self):
		data = json.dumps(
			{
				"data": [
			{
				"type": "Book",
				"attributes": {
					"title": "Ullyses",
					"description": "greek myth",
					"count": 4,
					"subscription_cost": 40,
					"topic": "Magical Realism"
				},
				"relationships": {
					"author": {
						"meta": {
							"count": 1
						},
						"data": [
							{
								"type": "Author",
								"id": "1"
							}
						]
					}
				}
			}
			]}
		)
		response = self.client.post("/api/books/", data, content_type=CONTENT_TYPE)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_book_api(self):
		response = self.client.get("/api/books/",content_type=CONTENT_TYPE)
		self.assertEqual(response.status_code,status.HTTP_200_OK)




