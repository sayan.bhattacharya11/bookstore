import datetime
from django.test import TestCase
from books.models import Book, Author
from django.contrib.auth.models import User
from books.models import Subscriber
from  books.models import Subscription

CONTENT_TYPE = "application/vnd.api+json"


class SubscriberTest(TestCase):

	@classmethod
	def setUp(cls):
		user = User.objects.create(username="riemann")
		subscriber = Subscriber.objects.create(user=user, address="gjhgj", phone_number="7890987654")
		author1 = Author.objects.create(name='James Joyce', address="45 Leicester City")

		book = Book.objects.create(title="Ullyses", description="greek myth", count=4, subscription_cost=40,
		                           topic="Magical Realism")

		book.author.add(author1)
		Subscription.objects.create(subscriber=subscriber, book=book, borrowed_date="2020-08-04", amount_paid="30", days=4, returned=True)

	def test_check_subscriber(self):
		subscription = Subscription.objects.get(id=1)
		subscriber = subscription.subscriber
		self.assertEqual(subscriber.user.username, "riemann")

	def test_check_book(self):
		subscription = Subscription.objects.get(id=1)
		book = subscription.book
		self.assertEqual(book.title, "Ullyses")

	def test_check_date(self):
		format_str = '%Y%m%d %H:%M:%S'
		subscription = Subscription.objects.get(id=1)
		borrowed_date = subscription.borrowed_date
		my_time = datetime.time()
		borrowed_date = datetime.datetime.combine(borrowed_date, my_time)
		stored_date = str(borrowed_date).split("-")
		date = "".join(stored_date)
		datetime_obj = datetime.datetime.strptime(date, format_str)
		self.assertEqual(borrowed_date,datetime_obj)

	def test_valid_date(self):
		subscription = Subscription.objects.get(id=1)
		borrowed_date = subscription.borrowed_date
		my_time = datetime.time()
		borrowed_date = datetime.datetime.combine(borrowed_date, my_time)
		self.assertLess(borrowed_date, datetime.datetime.now())

	def test_amount_paid(self):
		subscription_amount = Subscription.objects.get(id=1)
		amount  = subscription_amount.amount_paid
		self.assertNotEqual(amount, 0)
