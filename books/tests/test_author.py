import  json
from rest_framework.test import APITestCase
from rest_framework import  status
from django.test import TestCase
from books.models import Author

CONTENT_TYPE = "application/vnd.api+json"


class AuthorTest(TestCase):

	@classmethod
	def setUp(cls) -> None:
		author = Author.objects.create(name='Alan Mulaly',address="45 Leicester City")

	def test_name(self):
		author = Author.objects.get(id=1)
		name = author.name
		self.assertEquals(name, 'Alan Mulaly')

	def test_create_author(self):
		data = json.dumps(
			{"data": {
				"type": "Author",
				"attributes": {
					"name": "Salman Rushdie",
					"address": "20, Norfolk Rd"
				}
			}
			}
	)
		response = self.client.post("/api/authors/", data,content_type=CONTENT_TYPE)
		self.assertEqual(response.status_code,status.HTTP_201_CREATED)

	def test_address_length(self):
		author = Author.objects.get(id=1)
		max_length = author._meta.get_field('address').max_length
		self.assertEquals(max_length, 100)

	def test_author_api(self):
		response = self.client.get("/api/authors/",content_type=CONTENT_TYPE)
		self.assertEqual(response.status_code,status.HTTP_200_OK)

